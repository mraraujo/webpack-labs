# Skelton Webpack Frontend App

Veeery simple skelton webpack application.

### Requirements
 - node 8.x+
 - npm
 - webpack@2.2.0-rc.0
 - babel-core
 - babel-loader
 - babel-preset-env
 - css-loader@0.26.1
 - style-loader@0.13.1 
 - extract-text-webpack-plugin@2.0.0-beta.4
 - url-loader
 - image-webpack-loader
 
Simple clone this repository and modify according your frontend app.

# Project Setup

## Initializing project with npm

Initializing the project with npm init command:

`$ npm init -y`

## Install webpack

Install webpack at 2.2.0-rc.0 (all the examples are under this version)

`$ npm install --save-dev webpack@2.2.0-rc.0`

## Install babel

Installing babel compiler

`$ npm install --save-dev babel-core babel-loader babel-preset-env`

## Install css loader

Install css-loader and style-loader

```
npm install --save-dev css-loader@0.26.1
    style-loader@0.13.1 
    extract-text-webpack-plugin@2.0.0-beta.4
```

## Install image loader

If you on Ubuntu 18 run:

`$ apt-get install nasm`

And then run:

`$ npm install --save-dev url-loader image-webpack-loader`


## Future

- Update to latest webpack version

